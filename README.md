# JavaScript Editors

- [ ] Collect all JavaScript text/code editor implementations
- [ ] Split the libraries into two groups: one that uses `execCommand` and one that doesn't

## Use `execCommand`

- [Pell](https://github.com/jaredreich/pell/blob/master/src/pell.js)

## Do not use `execCommand`
